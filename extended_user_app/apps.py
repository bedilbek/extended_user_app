from django.apps import AppConfig


class ExtendedUserAppConfig(AppConfig):
    name = 'extended_user_app'
