from django.urls import path

from extended_user_app.views import LoginView, HomeView

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('home/', HomeView.as_view(), name='home'),
]
