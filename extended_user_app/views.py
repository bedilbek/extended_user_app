from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import View


class LoginView(View):
    def get(self, request):
        return render(request, 'login.html', context={})

    def post(self, request):
        phone_number = request.POST['phone_number']
        password = request.POST['password']

        user = authenticate(request, username=phone_number, password=password)

        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('home'))
        else:
            return HttpResponseRedirect(reverse('login'))


class HomeView(View):
    def get(self, request):
        return HttpResponse('Hello, You are authenticated')
